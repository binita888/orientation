# Text Formatting in Markdown
Markdown is a text-to-HTML conversion tool for web writers.
Markdown allows us to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML).

## Headers
- Headers are created by adding number sign (#) infront of a word or phrase.
- The number of number signs (#) used corresponds to the heading level.
- All Heading Levels :
   ![All Heading Levels] (header.png)
   
- Headers cannot be made bold but they can be italicized as :
  ### _Header Level 3_
    
## Bold
  - To bold text, two asterisks or underscore should be added before and after a word or phrase such as :
  
      > - \*\*Markdown*\* gives **Markdown**
      > - \_\_Markdown\_\_ also gives __Markdown__
       
## Italic

- To italicize text, one asterisk or underscore should be added before and after a word or phrase such as :

    > - \*Text formatting\* gives *Text formatting*
    > - \_Text formatting\_ also gives _Text formatting_

## Bold and Italic
- To emphasize text with bold and italics at the same time, three asterisks or three underscore should be added before and after a word or phrase such as :

     > - Markdown is  \*\*\*really important\*\*\*.
     > - Markdown is \_\_\_really important\_\_\_.
      
    which has the output as : Markdown is ***really important***.
       
- Alternative ways :
     
      1. \*\*\_really important\_\*\*
      2.  \_\_\*really important \*\_\_
      3.  \*\_\_really important\_\_\*
      4. \_\*\*really important\*\*\_
             
## Links 
There are two types of links : Inline link and Reference link
#### Inline Link
- To create inline links, link text should be enclosed inside the square brackets and then should be followed immediately with the URL in parenthesis such as :

       [Go for it ](www.markdowntutorial.com) 
       
which only shows  [Go for it](www.markdowntutorial.com) as a link to page markdowntutorial.

- Emphasis can be added to link text such as :
[This is **really amazing** tutorial.](www.markdowntutorial.com)
- Links can be made within headings too such as
    #### The latest tutorial from the [markdowntutorial](www.markdowntutorial.com)
    
#### Reference Link
- Reference link as the name implies, is the reference to another place in the document.
- Example :

    [hobbit-hole][1]    ( **First part of Link**)  
              :  
              :  
    [1]: https://en.wikipedia.org/wiki/Hobbit#Lifestyle   (**Second part of link**)
      
- Label[1] is used to point the link which is placed elsewhere in the document.
- Second part of the link can be placed anywhere in the document.

## Images
Images also have two styles : Inline Image link and Reference Image link

#### Inline Image
Inline image link is created by entering an exclamation point(!), wrapping the alt text in brackets( [ ] ) and then wrapping the link in parenthesis (  ( )  ) as below :

  \!\[Benjamin Bannekat](https://octodex.github.com/images/bannekat.png)

#### Reference Image
Reference Image link follows the same pattern as the reference link such as :

 \![Black cat][Black]  
     \![Orange cat][Orange]  
      	:  
      	:  
 \[Black]: https://upload.wikimedia.org/wikipedia/commons/a/a3/81_INF_DIV_SSI.jpg  
 \[Orange]: http://icons.iconarchive.com/icons/google/noto-emoji-animals-nature/256/22221-cat-icon.png

## Blockquotes
- It is a sentence or paragraph that has been specially formatted to draw attention to the reader.
- To create a block quote, the line should be prefaced with the greater than caret ( > ) such as :

\>  Markdown is lightweight  
    and it is so easy to learn.  
    It has various formatting types.

Above example has output as :  
>   Markdown is lightweight  
    and it is so easy to learn.  
    It has various formatting types.
    
- It is also useful if we have multiple paragraphs. Even blank line must contain the caret character such as :

\> Markdown is easy.  
\>  
\> Markdown is light weight.

## Lists
Three types of lists : Bullet list, Numbered list and Mixed list :

#### Unordered list (Bullet list )
- Preface each item in the list with asterisk (*) or hyphen (-) such as :

   \* Bullet 1  
   \* Bullet 2  
   		\* Intended Bullet  
   		\* Intended Bullet  
   \* Bullet 3
   
   which produces this :
  > 
   * Bullet 1  
   * Bullet 2  
       * Intended Bullet  
       * Intended Bullet  
   * Bullet 3

#### Ordered list (Numbered list)
- Preface each item in the list with number followed by period.
- Numbers don't have to be in numerical order but the first item should be 1. 
Example :

 1\. Item 1  
 1\. Item 2  
 2\. Item 3  
          1\. Intended Item  
          5\. Intended Item  
8\. Item 4

which produces :
>
  1.  Item 1  
  1.  Item 2  
  2.  Item 3  
       1. Intended Item  
       5. Intended Item  
  8. Item 4

#### Mixed list
Example :

1\. first  
2\. second  
1\. third  
     \* intended  
      \* intended  
2\. fourth

which produces :
>
  1. first  
  2. second  
  1. third  
       * intended  
       * intended  
   2. fourth

## Paragraphs
To achieve hard break on the paragraph, a new line should be insterted.
To achieve soft break, two spaces should be inserted after each new line.

> Do I contradict myself?·· Very well then I contradict myself,·· (I am large, I contain multitudes.)

Here, .. indicates two spaces. The result from above is :
>Do I contradict myself?  
Very well then I contradict myself,  
(I am large, I contain multitudes.)




       
