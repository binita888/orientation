# Getting Started with Gitlab
Gitlab is a great tool to manage git repositories on centralized server. It gives complete control over our repositories or projects.

## Features :
- Hosts our software projects for free.
- Offers free public and private repositories, issue-tracking and wikis.
- User friendly which increases the speed of working with Git.
- Provides its own Continuous Intigration (CI) system for managing the projects and provides user interface alon with other features of GitLab.

## Setting up Gitlab
After we have signed up and logged in Gitlab successfully, the following dashboard appears :
  ![gitlab dashboard] (welcome.png)

Features available in the dashboard are :
 

**Create a project :** By clicking this button, we can create our first project.
![Features on dashboard](project.png)

**Create a group :**  With this feature, we can create groups which let us bunch our project together, helps us to organize our projects and grant access to multiple projects more quickly.

**Explore public projects :** We can access huge library of public Gitlab projects here.

**Learn more about Gitlab :** Here we can access Gitlab's documentaion. It covers everything from creating a project to CI/CD pipelines.


### Creating a new project
By clicking on **Create a project** button, we can kickstart our process. This button has following options to choose from :

![Creating new project](newproject.png)

**Blank project :**
We get three levels of visibility. Aside from the regularly seen private and public, they offer internal, of which openness is limited to logged in user only.

**Create from a template :**
Gitlab offers Ruby on Rails, Spring and NodeJS Express templates to start with.

**Import project from other services :**
If we’ve got code on another platform, we can import it here. GitLab supports importing from GitHub, Bitbucket, and Google Code among others.

**CI/CD for external repo :**
This lets us connect external repositories to GitLab, using a GitLab project only for CI/CD pipelines.

#### Creating a Blank Project
- The project slug field will be auto-filled with whatever we enter for our project’s name. We can also change the project slug without affecting the project name.

- The Project URL field is automatically completed with our username. If we want to house this project under a group instead, we can switch that here through the dropdown menu.

- It’s also possible to give our project a description — useful for differentiating between similarly-named projects.

- Next, we’ll need to choose a visibility level for our project.

    **Public :** Public projects are visible to everyone on the internet and any one can copy our project.
    
    **Private :** Private project must be granted to each user explicitly.
    
   After creating a blank project, we get the following screen :
   
  ![Blank project screen](infinity.png)
   
   Now we have created our first project, so we can start coding in Gitlab, push some code, collaborate with the rest of dev team or keep an eye on the team’s progress.
