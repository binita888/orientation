# Version control with Git
* Git is a version control system for tracking changes in computer files.
*  Initially created by Linus Torvalds in 2005 who was also the creator of Linux.

### Features :
1. **Distributed version control**
      Many developers can work on a single project without having to be on the same network.

2. **Coordinates work between multiple developers**

3. **Tracks every single version and every single change that is made on the system or on the project.**

4. **Reverts back to any specific versions of any file at any time as long as it was committed to the repository.**

## Git Vocabulary 

#### Repository  : 
It is a collection of files and folders of various versions of a project which are tracked by using version control git.

#### Gitlab : 
Gitlab is a system or tool for managing git repositories.

#### Commit : 
Commit is the heart of Git usage . It is a term used for saving changes. Commit command saves the changes only on the local repo. It is like a snapshot of the project where the new version of project is created in the current repository.


#### Push :
After commiting all the changes, git push command moves the changes from the local repository to the remote server.

#### Branch : 
Branch in Git is similar to the branch of tree. The default branch in git is called the Master branch(similar to ther tree trunk). Git is a way to keep developing and coding a new feature or modification to the software and still not affecting the main part of the project. 

#### Merge : 
Merge operation is used to combine multiple commits into one single history. 

#### Clone : 
Cloning is a process of creating an identical copy of a Git Remote Repository to the local machine.

#### Fork : 
Fork is a copy of a repository.  Forking in GitHub is a process of creating a copy of a complete repository to the user’s GitHub Account from another account. 


## Getting Started and Required Commands
###  Installing git :
- For linux, open terminal and type  :
> **$ sudo apt-get install git**
- For ubuntu, git is pre-installed.
- For windows, [download the installer](https://git-scm.com/download/win) and run it.
- For Mac, open the terminal and type **git**.

### Git Internals  
Every project under the distributed version control sytem Git goes through three stages :

* **Modified** : Changes are made to the local repo but are not yet commited.

* **Staged** :  All the modified files are added to git's version control but changes have not been committed.

* **Committed** : 
      - Files or data are safely stored in the local repo as a part of   modification. 
      -  Each commit represents the changes made to local repo in the past, with the details about the time at which commit was made and the author of the code. 

### Different trees in the repository :
* **Workspace** :
 It is a working directory which consists of files which we are currently working on.
 It is a file system where we can view and modify files.
 
 * **Staging** :
 All the staged files goes into this tree of repo.
 
 * **Local Repository**
 All the committed files are placed into this tree of repo.
 
 * **Remote Repository**
 This is the repo which is placed on the internet. All the changes committed in local repository are finnally reflected on remote repository by pushing it to this tree.
 
### Git Workflow
* Clone the repo

          $ git clone < link-to-repository >
   
* Creat a new branch

         $ git checkout mater
         $ git checkout -b create < your-branch-name >

* Adding changes to the staging area
   
          $ git add .
          
* Committing
Takes the snapshot or pictures of the files in staging area and store permanently in the local git repository.

        $ git commit -m " < commit message > "
              
* Pushing
Takes the files as they are in the local git repo and stores the snapshot of them permanently in the Remote repository.

        $ git push origin < branch-name >
 
